__author__ = 'alex'

import json

class PayLoad(dict):
    def __init__(self, data=None):
        self['error'] = 0
        self['payload'] = data
        self['count'] = self.count

    @property
    def payload_type(self):
        return type(self['payload'])

    @property
    def count(self):
        return len(self['payload']) if self.payload_type is list else 1

    def add_payload(self, payload, name='payload'):
        if not self[name]:
            self[name] = payload
        else:
            if isinstance(self[name], list):
                self[name].append(payload)
            elif isinstance(self[name], dict):
                swp = self[name]
                self[name] = list().append(swp)
        self['count'] += 1


    def add_error(self, exception):
        self['error'] = 1
        self.update(exception.to_dict())
        return self
        #self['error_message'] = msg
        #self['error_code'] = code

    @property
    def json(self):
        return json.dumps(self)

    def __str__(self):
        return json.dumps(self)



