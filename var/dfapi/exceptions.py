__author__ = 'alex'

class BaseAPIError(Exception):
    code = 0
    msg = ""

    def to_dict(self):
        return {"error_code": self.code, "error_message": self.msg}

class NotValidParameters(BaseAPIError):
    msg = "Passed parameters are not valid."
    code = 1000


class MethodDoesNotExist(BaseAPIError):
    msg = "API method does not exist"
    code = 1001