__author__ = 'alex_bojko'

import tornado.ioloop
import tornado.web
import dfapi
from dfapi import exceptions
import json
from dfapi.files import files
from dfapi.payload import PayLoad


API_CLASSES = {
    "files": files.Files(),
}


class MainHandler(tornado.web.RequestHandler):

    def get(self, *args, **kwargs):
        self._handle(*args, **kwargs)

    def post(self, *args, **kwargs):
        self._handle(*args, **kwargs)

    def _handle(self, cls_name='', method_name=''):
        response = PayLoad()
        if cls_name in API_CLASSES:
            cls = API_CLASSES[cls_name]
            if hasattr(cls, method_name):
                method = getattr(cls, method_name, None)
                if method is not None:
                    str_data = self.get_argument('data', None)
                    data = json.loads(str_data)
                    resp = method(data)
                    response.add_payload(resp)
                else:
                    response.add_error(exceptions.MethodDoesNotExist())
            else:
                response.add_error(exceptions.MethodDoesNotExist())
        self.write(response.json)


application = tornado.web.Application([
    (r"/api/(?P<cls_name>[^\/]+)/(?P<method_name>[^\/]+)/", MainHandler),
])

if __name__ == "__main__":
    application.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

